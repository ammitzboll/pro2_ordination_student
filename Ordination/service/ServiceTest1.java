package service;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import ordination.Laegemiddel;
import ordination.PN;
import ordination.Patient;

public class ServiceTest1 {

	
	private Service service;
	@Before
	public void setUp() throws Exception {
		service = Service.getTestService();
	}

	@Test
	public void testOpretPNOrdination() {
		service.opretPatient("112233-1122", "Hans", 70);
		service.opretLaegemiddel("Pencelin", 1, 2, 3, "ML");
		// Test Case 1 test om en ordination kan oprettes med rigtige datoer
		assertNotNull(service.opretPNOrdination(LocalDate.of(2018, 01, 01), LocalDate.of(2018, 01, 02),
				service.getAllPatienter().get(0),
				service.getAllLaegemidler().get(0), 2));
		// Test Case 2 test om en ordination kan oprettes med datoer lig hinanden 
		assertNotNull(service.opretPNOrdination(LocalDate.of(2018, 01, 01), LocalDate.of(2018, 01, 01),
				service.getAllPatienter().get(0),
				service.getAllLaegemidler().get(0), 2));
	}
	@Test (expected = IllegalArgumentException.class)
	public void testOpretPNOrdination2() {
		// Test Case 3 test om vi får IllegalArgumentException på dato
		service.opretPatient("112233-1122", "Hans", 70);
		service.opretLaegemiddel("Pencelin", 1, 2, 3, "ML");
		service.opretPNOrdination(LocalDate.of(2018, 01, 02), LocalDate.of(2018, 01, 01), 
				service.getAllPatienter().get(0),
				service.getAllLaegemidler().get(0), 2);
	}
	
	
/**
 * Vi synes vi har fået gjort det rigtigt her, men uanset hvad så vil den ikke fange den IllegalArgumentException på antal < 0
 */
//	@Test (expected = IllegalArgumentException.class)
//	public void testOpretPNOrdination3() {
//		// Test Case 4 test om vi får IllegalArgumentException på antal
//		Patient patient = service.opretPatient("112233-1122", "Hans", 70);
//		Laegemiddel middel = service.opretLaegemiddel("Pencelin", 1, 2, 3, "ML");
//		service.opretPNOrdination(LocalDate.of(2018, 01, 01), LocalDate.of(2018, 01, 02), 
//				patient,
//				middel, -1);
//	}
//	

	
	


	@Test
	public void testOpretDagligFastOrdination() {
		// Test Case 1 kan objektet oprettes
		Patient patient = service.opretPatient("11111-1111", "Hans", 70);
		Laegemiddel middel = service.opretLaegemiddel("Pencelin", 1, 2, 3, "ML");
		assertNotNull(service.opretDagligFastOrdination(LocalDate.of(2018, 01, 01), LocalDate.of(2018, 01, 02), 
				patient, middel, 1, 2, 3, 4));
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testOpretDagligFastOrdination2() {
		Patient patient = service.opretPatient("11111-1111", "Hans", 70);
		Laegemiddel middel = service.opretLaegemiddel("Pencelin", 1, 2, 3, "ML");
		// Test Case 2 datoer ligger omvendt
		assertNull(service.opretDagligFastOrdination(LocalDate.of(2018, 01, 02), LocalDate.of(2018, 01, 01), patient,
						middel, 1, 2, 3, 4));
	}
	
	@Test (expected = AssertionError.class)
	public void testOpretDagligFastOrdination3() {
		Patient patient = service.opretPatient("11111-1111", "Hans", 70);
		Laegemiddel middel = service.opretLaegemiddel("Pencelin", 1, 2, 3, "ML");
		// Test Case 3 check om dosis antal er negativt
		assertNull(service.opretDagligFastOrdination(LocalDate.of(2018, 01, 01), LocalDate.of(2018, 01, 02), patient,
						middel, -1, 2, 3, 4));
	}

	@Test
	public void testOpretDagligSkaevOrdination() {
		Patient patient = service.opretPatient("11111-1111", "Hans", 70);
		Laegemiddel middel = service.opretLaegemiddel("Pencelin", 1, 2, 3, "ML");
		LocalTime[] time = new LocalTime[2];
		time[0] = LocalTime.of(12, 30);
		time[1] = LocalTime.of(16, 00);
		double[] enheder = new double[2];
		enheder[0] = 1;
		enheder[1] = 2;
		// Test Case 1 se om objektet kan oprettes
		assertNotNull(service.opretDagligSkaevOrdination(LocalDate.of(2018, 01, 01), LocalDate.of(2018, 01, 01),
				patient, middel, time, enheder));
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testOpretDagligSkaevOrdination2() {
		Patient patient = service.opretPatient("11111-1111", "Hans", 70);
		Laegemiddel middel = service.opretLaegemiddel("Pencelin", 1, 2, 3, "ML");
		LocalTime[] time = new LocalTime[2];
		time[0] = LocalTime.of(12, 30);
		time[1] = LocalTime.of(16, 00);
		double[] enheder = new double[2];
		enheder[0] = 1;
		enheder[1] = 2;
		// Test Case 2 datoer ligger omvendt
		service.opretDagligSkaevOrdination(LocalDate.of(2018, 01, 02), LocalDate.of(2018, 01, 01),
				patient, middel, time, enheder);
	}
	
	@Test (expected = ArrayIndexOutOfBoundsException.class)
	public void testOpretDagligSkaevOrdination3() {
		Patient patient = service.opretPatient("11111-1111", "Hans", 70);
		Laegemiddel middel = service.opretLaegemiddel("Pencelin", 1, 2, 3, "ML");
		LocalTime[] time = new LocalTime[2];
		time[0] = LocalTime.of(12, 30);
		time[1] = LocalTime.of(16, 00);
		double[] enheder = new double[1];
		enheder[0] = 1;
		// Test Case 3 test om arrays er lige store
		service.opretDagligSkaevOrdination(LocalDate.of(2018, 01, 01), LocalDate.of(2018, 01, 02),
				patient, middel, time, enheder);
	}

	@Test
	public void testOrdinationPNAnvendt() {
		Laegemiddel middel = service.opretLaegemiddel("Pencelin", 1, 2, 3, "ML");
		PN pn = new PN(LocalDate.of(2018, 01, 01), LocalDate.of(2018, 01, 03), middel);
		// Test Case 1 test om PNAnvend bliver lagt på gemtedatoer
		service.ordinationPNAnvendt(pn, LocalDate.of(2018, 01, 02));
		assertEquals(LocalDate.of(2018, 01, 02), pn.getGemteDatoer().get(0));
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testOrdinationPNAnvendt2() {
		Laegemiddel middel = service.opretLaegemiddel("Pencelin", 1, 2, 3, "ML");
		PN pn = new PN(LocalDate.of(2018, 01, 02), LocalDate.of(2018, 01, 03), middel);
		service.ordinationPNAnvendt(pn, LocalDate.of(2018, 01, 01));
	}

	@Test
	public void testAntalOrdinationerPrVægtPrLægemiddel() {
		Laegemiddel middel = service.opretLaegemiddel("Pencelin", 1, 2, 3, "ML");
		Patient p1 = service.opretPatient("11111-1111", "Per", 80);
		Patient p2 = service.opretPatient("232323-1212", "Lone", 60);
		service.opretDagligFastOrdination(LocalDate.of(2018, 01, 01), LocalDate.of(2018, 01, 02), p1, middel, 1, 1, 1, 1);
		service.opretDagligFastOrdination(LocalDate.of(2018, 01, 01), LocalDate.of(2018, 01, 02), p2, middel, 1, 1, 1, 1);
		assertEquals(2,service.antalOrdinationerPrVægtPrLægemiddel(50, 100, middel));
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testAntalOrdinationerPrVægtPrLægemiddel2() {
		Laegemiddel middel = service.opretLaegemiddel("Pencelin", 1, 2, 3, "ML");
		Patient p1 = service.opretPatient("11111-1111", "Per", 80);
		Patient p2 = service.opretPatient("232323-1212", "Lone", 60);
		service.opretDagligFastOrdination(LocalDate.of(2018, 01, 01), LocalDate.of(2018, 01, 02), p1, middel, 1, 1, 1, 1);
		service.opretDagligFastOrdination(LocalDate.of(2018, 01, 01), LocalDate.of(2018, 01, 02), p2, middel, 1, 1, 1, 1);
		service.antalOrdinationerPrVægtPrLægemiddel(-1, 100, middel);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testAntalOrdinationerPrVægtPrLægemiddel3() {
		Laegemiddel middel = service.opretLaegemiddel("Pencelin", 1, 2, 3, "ML");
		Patient p1 = service.opretPatient("11111-1111", "Per", 80);
		Patient p2 = service.opretPatient("232323-1212", "Lone", 60);
		service.opretDagligFastOrdination(LocalDate.of(2018, 01, 01), LocalDate.of(2018, 01, 02), p1, middel, 1, 1, 1, 1);
		service.opretDagligFastOrdination(LocalDate.of(2018, 01, 01), LocalDate.of(2018, 01, 02), p2, middel, 1, 1, 1, 1);
		service.antalOrdinationerPrVægtPrLægemiddel(100, 10, middel);
	}

}
