package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public class DagligSkaev extends Ordination {

    private List<Dosis> dosis = new ArrayList<>();

    public DagligSkaev(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel, Patient patient,
            LocalTime[] klokkeslaet, double[] antalEnheder) {
        super(startDen, slutDen, laegemiddel);

        for (int i = 0; i < klokkeslaet.length; i++) {
            opretDosis(klokkeslaet[i], antalEnheder[i]);
        }
    }

    public void opretDosis(LocalTime tid, double antal) {
        dosis.add(new Dosis(tid, antal));
    }

    @Override
    public double samletDosis() {
        return doegnDosis() * super.antalDage();
    }

    @Override
    public double doegnDosis() {
        double antalDosis = 0;
        for (int i = 0; i < dosis.size(); i++) {
            antalDosis += dosis.get(i).getAntal();
        }
        return antalDosis;
    }

    @Override
    public String getType() {

        return "Daglig Skaev";
    }

    public List<Dosis> getDosis() {
        return new ArrayList<Dosis>(dosis);
    }

}
