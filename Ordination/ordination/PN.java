package ordination;

import java.time.LocalDate;
import java.util.ArrayList;

public class PN extends Ordination {

    private double antalEnheder = 0;
    private int count = 0;
    private LocalDate foersteDosisDag;
    private LocalDate sidsteDosisDag;
    private ArrayList<LocalDate> gemteDatoer = new ArrayList<>();

    public PN(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel) {
        super(startDen, slutDen, laegemiddel);

        this.foersteDosisDag = startDen;
        this.sidsteDosisDag = slutDen;
    }

    /**
     * Registrerer at der er givet en dosis paa dagen givesDen Returnerer true hvis
     * givesDen er inden for ordinationens gyldighedsperiode og datoen huskes
     * Retrurner false ellers og datoen givesDen ignoreres
     *
     * @param givesDen
     * @return
     */
    public boolean givDosis(LocalDate givesDen, double antal) {
        boolean supplied = false;
        if (count == 0) {
            foersteDosisDag = givesDen;
        }

        if ((givesDen.isBefore(getSlutDen()) || givesDen.equals(getSlutDen()))
                && (givesDen.isAfter(getStartDen()) || givesDen.equals(getStartDen()))) {
            supplied = true;
            gemteDatoer.add(givesDen);
            antalEnheder += antal;
            count++;
            sidsteDosisDag = givesDen;

        }

        return supplied;
    }

    public ArrayList<LocalDate> getGemteDatoer() {
        return new ArrayList<>(gemteDatoer);
    }

    public void addGemteDatoer(LocalDate dato) {
        gemteDatoer.add(dato);
    }

    @Override
    public double doegnDosis() {
        double result = gemteDatoer.size() * antalEnheder;
        return result / super.antalDage() * 1.0;
    }

    @Override
    public double samletDosis() {

        return antalEnheder * gemteDatoer.size();
    }

    /**
     * Returnerer antal gange ordinationen er anvendt
     *
     * @return
     */
    public int getAntalGangeGivet() {

        return this.count;
    }

    public double getAntalEnheder() {
        return antalEnheder;
    }

    @Override
    public String getType() {

        return "PN";
    }
}
