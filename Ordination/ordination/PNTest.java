package ordination;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

public class PNTest {

    private Laegemiddel lMiddel;

    @Before
    public void setUp() throws Exception {
        lMiddel = new Laegemiddel("Paracetamol", 1, 2, 3, "ML");
    }

    @Test
    public void testPN() {
        // Test Case 1 - forløb på 2 dage
        PN pn1 = new PN(LocalDate.of(2018, 01, 01), LocalDate.of(2018, 01, 02), lMiddel);
        assertNotNull(pn1);
        // Test Case 2 - forløb på 1 dag
        PN pn2 = new PN(LocalDate.of(2018, 01, 01), LocalDate.of(2018, 01, 01), lMiddel);
        assertNotNull(pn2);
    }

    @Test
    public void testGivDosis() {
        // Test Case 1 - tjek om det virker
        PN pn1 = new PN(LocalDate.of(2018, 01, 01), LocalDate.of(2018, 01, 02), lMiddel);
        assertEquals(true, pn1.givDosis(LocalDate.of(2018, 01, 01), 1));
        // Test Case 2 - error
        PN pn2 = new PN(LocalDate.of(2018, 01, 02), LocalDate.of(2018, 01, 03), lMiddel);
        assertEquals(false, pn2.givDosis(LocalDate.of(2018, 01, 01), 1));
        // Test Case 3 - error
        PN pn3 = new PN(LocalDate.of(2018, 01, 02), LocalDate.of(2018, 01, 03), lMiddel);
        assertEquals(false, pn3.givDosis(LocalDate.of(2018, 01, 04), 1));

    }

    @Test
    public void testDoegnDosis() {
        // Test Case 1
        PN pn1 = new PN(LocalDate.of(2018, 01, 01), LocalDate.of(2018, 01, 02), lMiddel);
        pn1.givDosis(LocalDate.of(2018, 01, 01), 1);
        assertEquals(0.5, pn1.doegnDosis(), 0.01);
        // Test Case 2 - grænseværdi
        PN pn2 = new PN(LocalDate.of(2018, 01, 01), LocalDate.of(2018, 01, 01), lMiddel);
        pn2.givDosis(LocalDate.of(2018, 01, 01), 1);
        assertEquals(1, pn2.doegnDosis(), 0.01);

    }

    @Test
    public void testSamletDosis() {
        // Test Case 1
        PN pn1 = new PN(LocalDate.of(2018, 01, 01), LocalDate.of(2018, 01, 02), lMiddel);
        pn1.givDosis(LocalDate.of(2018, 01, 01), 1);
        pn1.givDosis(LocalDate.of(2018, 01, 02), 1);
        assertEquals(2, pn1.doegnDosis(), 0.01);

    }

}
