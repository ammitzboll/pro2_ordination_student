package ordination;

import java.time.LocalDate;
import java.time.LocalTime;

public class DagligFast extends Ordination {

    private Dosis[] dosis = new Dosis[4];

    
    public DagligFast(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel, double morgenAntal,
            double middagAntal, double aftenAntal, double natAntal) {
        super(startDen, slutDen, laegemiddel);
        
        this.dosis[0] = new Dosis(LocalTime.of(6, 0), morgenAntal);
        this.dosis[1] = new Dosis(LocalTime.of(12, 0), middagAntal);
        this.dosis[2] = new Dosis(LocalTime.of(18, 0), aftenAntal);
        this.dosis[3] = new Dosis(LocalTime.of(2, 0), natAntal);
    }

    @Override
    public double samletDosis() {
        double antal = 0;

        antal = doegnDosis() * super.antalDage();
        return antal;
    }

    @Override
    public double doegnDosis() {
        double antal = 0;
        for (int i = 0; i < dosis.length; i++) {
            antal += dosis[i].getAntal();
        }
        return antal;
    }

    @Override
    public String getType() {
        return "Daglig Fast";
    }

    public Dosis[] getDosis() {
        return dosis;
    }

}
