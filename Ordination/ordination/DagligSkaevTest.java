package ordination;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

public class DagligSkaevTest {
	
	Laegemiddel lMiddel;
	Patient p1;

	@Before
	public void setUp() throws Exception {
		lMiddel = new Laegemiddel("Pencilin",1, 2, 3, "ML");
		p1 = new Patient("20011990", "Hans", 70);
	}

	@Test
	public void testSamletDosis() {
		LocalTime[] klok = new LocalTime[3];
		klok[0] = LocalTime.of(12, 30);
		klok[1] = LocalTime.of(16, 00);
		klok[2] = LocalTime.of(22, 30);
		double[] enheder = new double[3];
		enheder[0] = 1;
		enheder[1] = 2;
		enheder[2] = 3;
		// Test Case 1 med 2 dage
		DagligSkaev ds1 = new DagligSkaev(LocalDate.of(2018, 01, 01), LocalDate.of(2018, 01, 02), lMiddel,
				p1, klok, enheder);
		assertEquals(12, ds1.samletDosis(), 0.01);
		// Test Case 2 på grænse værdien
		DagligSkaev ds2 = new DagligSkaev(LocalDate.of(2018, 01, 01), LocalDate.of(2018, 01, 01), lMiddel,
				p1, klok, enheder);
		assertEquals(6, ds2.samletDosis(),0.01);
	}

	@Test
	public void testDoegnDosis() {
		LocalTime[] klok = new LocalTime[3];
		klok[0] = LocalTime.of(12, 30);
		klok[1] = LocalTime.of(16, 00);
		klok[2] = LocalTime.of(22, 30);
		double[] enheder = new double[3];
		enheder[0] = 1;
		enheder[1] = 2;
		enheder[2] = 3;
		// Test Case 1 om doegndosis fungere som den skal og bliver oprettet
		DagligSkaev ds1 = new DagligSkaev(LocalDate.of(2018, 01, 01), LocalDate.of(2018, 01, 02), lMiddel,
				p1, klok, enheder);
		assertEquals(6, ds1.doegnDosis(), 0.01);
		assertEquals(LocalDate.of(2018, 01, 01), ds1.getStartDen());
		// Test Case 2 opret lige på grænse værdien
		DagligSkaev ds2 = new DagligSkaev(LocalDate.of(2018, 01, 01), LocalDate.of(2018, 01, 01), lMiddel, 
				p1, klok, enheder);
		assertNotNull(ds2);
	}

	@Test
	public void testDagligSkaev() {
		LocalTime[] klok = new LocalTime[3];
		klok[0] = LocalTime.of(12, 30);
		klok[1] = LocalTime.of(16, 00);
		klok[2] = LocalTime.of(22, 30);
		double[] enheder = new double[3];
		enheder[0] = 1;
		enheder[1] = 2;
		enheder[2] = 3;
		// Test Case 1 objektet oprettes
		DagligSkaev ds1 = new DagligSkaev(LocalDate.of(2018, 01, 01), LocalDate.of(2018, 01, 02), lMiddel,
				p1,klok, enheder );
		assertNotNull(ds1);
		assertEquals(LocalDate.of(2018, 01, 01), ds1.getStartDen());
		assertEquals(LocalDate.of(2018, 01, 02), ds1.getSlutDen());
		// Test Case 2 grænse værdi for dato
		DagligSkaev ds2 = new DagligSkaev(LocalDate.of(2018, 01, 01), LocalDate.of(2018, 01, 01), lMiddel,
				p1, klok, enheder);
		assertNotNull(ds2);
	}

	@Test
	public void testOpretDosis() {
		LocalTime[] klok = new LocalTime[3];
		klok[0] = LocalTime.of(12, 30);
		klok[1] = LocalTime.of(16, 00);
		klok[2] = LocalTime.of(22, 30);
		double[] enheder = new double[3];
		enheder[0] = 1;
		enheder[1] = 2;
		enheder[2] = 3;
		// Test Case 1 test tider samt antal om det passer, vi bruger opretDosis igennem construtoren.
		DagligSkaev ds1 = new DagligSkaev(LocalDate.of(2018, 01, 01),LocalDate.of(2018, 01, 02), lMiddel, 
				p1, klok, enheder);
		assertEquals(1, ds1.getDosis().get(0).getAntal(), 0.01);
		assertEquals(LocalTime.of(12, 30), ds1.getDosis().get(0).getTid());
		assertEquals(3, ds1.getDosis().get(2).getAntal(), 0.01);
		assertEquals(LocalTime.of(22, 30), ds1.getDosis().get(2).getTid());
	}

}
