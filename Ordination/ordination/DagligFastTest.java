package ordination;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;

public class DagligFastTest {
	
	private Laegemiddel lMiddel;
	
	
	

	@Before
	public void setUp() throws Exception {
		lMiddel = new Laegemiddel("Pencilin", 1,2,3, "ML");
	}

	@Test
	public void testSamletDosis() {
		// Test Case 1 objektet bliver oprettet
		DagligFast df1 = new DagligFast(LocalDate.of(2018, 01, 01), LocalDate.of(2018, 01, 02), lMiddel,
				0,0,0,1);
		assertEquals(2, df1.samletDosis(), 0.01);
		// Test Case 2 objektet bliver oprettet
		DagligFast df2 = new DagligFast(LocalDate.of(2018, 01, 01), LocalDate.of(2018, 01, 02), lMiddel,
				1,0,4,3);
		assertEquals(16, df2.samletDosis(),0.01);
		// Test Case 3 Gr�nse v�rdi
		DagligFast df3 = new DagligFast(LocalDate.of(2018, 01, 01), LocalDate.of(2018, 01, 01), lMiddel,
				0,0,0,1);
		assertEquals(1, df3.samletDosis(), 0.01);
	}

	@Test
	public void testDoegnDosis() {
		// Test case 1 objektet createt med 0 som gr�nse v�rdi
		DagligFast df1 = new DagligFast(LocalDate.of(2018, 01, 01), LocalDate.of(2018, 01, 02), lMiddel,
				0,0,0,0);
		assertEquals(0, df1.doegnDosis(), 0.01);
		// Test case 2 object createt med 1 
		DagligFast df2 = new DagligFast(LocalDate.of(2018, 01, 01), LocalDate.of(2018, 01, 02), lMiddel,
				1,1,1,1);
		assertEquals(4, df2.doegnDosis(), 0.01);	
	}
	
	@Test
	public void testGetType() {
		// Test Case 1 kommer den rigtige type tilbage
		DagligFast df1 = new DagligFast(LocalDate.of(2018, 01, 01), LocalDate.of(2018, 01, 02), lMiddel,
				1,1,1,1);
		assertEquals("Daglig Fast", "Daglig Fast", df1.getType());
	}

	@Test
	public void testDagligFast() {
		// Test Case 1 vil objecktet blive oprettet
		DagligFast df = new DagligFast(LocalDate.of(2018, 01, 01), LocalDate.of(2018, 01, 02), 
				lMiddel, 1, 2, 3, 4);
		assertNotNull(df);
		assertEquals(df.getStartDen(), LocalDate.of(2018, 01, 01));
		assertEquals(df.getSlutDen(), LocalDate.of(2018, 01, 02));
		// Test Case 2 samme dato 
		DagligFast df2 = new DagligFast(LocalDate.of(2018, 01, 01), LocalDate.of(2018, 01, 01),
				lMiddel, 1, 2, 3, 4);
		assertNotNull(df2);
		assertEquals(df2.getStartDen(), LocalDate.of(2018, 01, 01));
		assertEquals(df2.getSlutDen(), LocalDate.of(2018, 01, 01));
	}

	@Test
	public void testGetDosis() {
		// Test Case 1 
		DagligFast df = new DagligFast(LocalDate.of(2018, 01, 01), LocalDate.of(2018, 01, 02), lMiddel,
				1,1,1,1);
		assertEquals(1, df.getDosis()[0].getAntal(), 0.01);
		assertEquals(1, df.getDosis()[1].getAntal(), 0.01);
		assertEquals(1, df.getDosis()[2].getAntal(), 0.01);
		assertEquals(1, df.getDosis()[3].getAntal(), 0.01);
	}

}
